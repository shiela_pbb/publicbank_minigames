﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjecMove : MonoBehaviour
{
    public float startPosition;
    public float travelTimeDuration;

    private IEnumerator thisCouroutine;

    public void OnEnable()
    {
        startPosition = transform.position.y;
        thisCouroutine = MoveObject();
        StartCoroutine(MoveObject());
        EndLessRunnerTimer.onGameOver += OnGameOver;
    }

    public void OnDisable()
    {
        StopCoroutine(MoveObject());
    }


    // Update is called once per frame
    private IEnumerator MoveObject()
    { 
        float elapsedTime = 0;

        while (elapsedTime < travelTimeDuration)
        {
            float _elapsedTime = elapsedTime / travelTimeDuration;

            transform.position = new Vector3(transform.position.x,
                                     Mathf.Lerp(startPosition, -startPosition, _elapsedTime),
                                     transform.position.z);
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.position = new Vector3(transform.position.x, -startPosition, transform.position.z);

        StopCoroutine(MoveObject());
    }

    void OnGameOver()
    {

    }
}
