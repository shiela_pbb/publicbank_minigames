﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDespawner : MonoBehaviour
{
    public GameObject explosionPrefab;
    public List<string> objectDespawnerTag;

    public delegate void OnObjectDespawn(GameObject obj);
    public static event OnObjectDespawn onObjectDespawn;

    private void OnTriggerEnter2D(Collider2D col)
    {

        for (int i = 0; i < objectDespawnerTag.Count; i++ )
        { 
            if (col.gameObject.tag == objectDespawnerTag[i])
            {
                gameObject.SetActive(false);

                if (col.tag == "Despawner")
                    return;

                onObjectDespawn.Invoke(gameObject);

            }
        }

    }

}
