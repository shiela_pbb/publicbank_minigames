﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class EndlessRunnerGameController : MonoBehaviour
{

    public EndLessRunnerTimer timer;
    public GameObject play;
    public GameObject gameOverHud;
    public string coinTag;
    public string asteroid;

    public delegate void OnStartGame();
    public static event OnStartGame onStartGame;

    public void OnGameStart()
    {

        gameOverHud.transform.localPosition = new Vector3(0f, 2500f, 0f);
        if (onStartGame != null)
        {
            Debug.Log("Method : " + onStartGame.Method);
            onStartGame.Invoke();
        }

        Time.timeScale = 1;

        EndLessRunnerTimer.onGameOver -= OnGame0ver;
        EndLessRunnerTimer.onGameOver += OnGame0ver;
    }

    void OnGame0ver()
    {
        play.SetActive(true);

        Time.timeScale = 0;

        gameOverHud.transform.localPosition = Vector3.zero;

    }
}
