﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AddScore : MonoBehaviour
{

    // Public Variables
    public TMP_Text score;
    public int scoreValue;
    public int scoreCount;
    public string audioNameToPlay;

    // Private Variables
    public void OnEnable()
    {
        EndlessRunnerGameController.onStartGame -= OnStartGame;
        EndlessRunnerGameController.onStartGame += OnStartGame;
    }

    public void InitializeScore()
    {
        score.text = "0";
        scoreCount = 0;
        ObjectDespawner.onObjectDespawn += OnAddScore;

        EndlessRunnerGameController.onStartGame -= OnStartGame;
        EndLessRunnerTimer.onGameOver += OnGameOver;
    }

    void OnStartGame()
    {
        InitializeScore();
    }

    void OnAddScore(GameObject obj)
    {

        if (obj.tag != "Coin")
            return;

        Debug.Log("Add Score game object : " + gameObject);
        scoreCount += scoreValue;
        score.text = scoreCount.ToString();
        AudioManager.instance.PlaySFX("Add_coin_8");
    }

    void OnGameOver()
    {
        ObjectDespawner.onObjectDespawn -= OnAddScore;
        EndlessRunnerGameController.onStartGame += OnStartGame;
        EndLessRunnerTimer.onGameOver -= OnGameOver;
    }

}
