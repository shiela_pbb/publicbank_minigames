﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SrollingBG : MonoBehaviour
{
    // Public Variables
    public float scrollSpeed;
    public float tileSize;
    public Vector3 startPos;

    // Private Variables
    private float newPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        newPos = Mathf.Repeat(Time.time * scrollSpeed, tileSize);
        transform.position = startPos + Vector3.down * newPos;
    }
}