﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class EndLessRunnerTimer : MonoBehaviour
{
    [System.Serializable]
    public class ObjectTags
    {
        public string tag;
        public float timeDeduction;
    }

    // Public Varialbes
    public TMP_Text timer;
    public float timeValue;

    public bool isPlaying;

    public List<ObjectTags> objectTags = new List<ObjectTags>();


    //Private Variables
    private float time;

    public delegate void OnGameOver();
    public static event OnGameOver onGameOver;


    public void Start()
    {
        EndlessRunnerGameController.onStartGame += StartTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            time -= Time.deltaTime;
            timer.text = Mathf.RoundToInt(time).ToString();


            if (time <= 0)
            {
               
                if (onGameOver != null)
                {
                    onGameOver.Invoke();
                    EndlessRunnerGameController.onStartGame += StartTimer;
                    ObjectDespawner.onObjectDespawn -= DeductTime;
                }
                isPlaying = false;
                timer.text = "0";
               
            }
        }
    }

    public void StartTimer()
    {
        EndlessRunnerGameController.onStartGame -= StartTimer;
        ObjectDespawner.onObjectDespawn += DeductTime;

        RestartTimer();
        isPlaying = true;
    }

    public void RestartTimer()
    {
        time = timeValue;
    }

    public void DeductTime(GameObject obj)
    {
        if (!obj.tag.Contains("Asteroid"))
            return;

        Debug.Log("DEDUCT TIME");
        for (int i = 0; i < objectTags.Count; i++) {

            if (obj.tag != objectTags[i].tag)
                return;

                time -= objectTags[i].timeDeduction;
        }/**/

    }

}
