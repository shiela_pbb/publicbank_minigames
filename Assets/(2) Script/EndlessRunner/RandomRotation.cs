﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{

    public float rotationSpeed;

    public void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(1, 3) * rotationSpeed;
        //gameObject.GetComponent<Rigidbody>().angularVelocity
    }

    private void FixedUpdate()
    {
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(1, 10) * rotationSpeed;
    }
}
