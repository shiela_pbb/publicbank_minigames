﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    //Public Variables
    public List <GameObject> prefab;
    public Transform parent;
    public int numberOfPrefabToSpawn;
    public Vector2 startPos;
    public Vector2 spawnPosition;
    public bool spawnByTime;
    public float intervalTime;

    //Private Variables
    private List<GameObject> objectPrefabs;
    private float timer;


    private void OnEnable()
    {
        EndlessRunnerGameController.onStartGame += StartSpawning;
        EndLessRunnerTimer.onGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        EndlessRunnerGameController.onStartGame -= StartSpawning;
    }

    private void OnDestroy()
    {
        EndlessRunnerGameController.onStartGame -= StartSpawning;
    }


    // Start is called before the first frame update
    void Start()
    {
        objectPrefabs = new List<GameObject>();
        for (int i = 0; i < numberOfPrefabToSpawn; i++)
        {
            int num = Random.Range(0, prefab.Count);
            GameObject obj = (GameObject)Instantiate(prefab[num], parent);
            SpriteRenderer _sprite = obj.GetComponent<SpriteRenderer>();
            _sprite.sortingOrder = Random.Range(2, 4);
            obj.SetActive(false);
            objectPrefabs.Add(obj);
        }

        startPos = new Vector2(parent.position.x, parent.position.y);
        Debug.Log("start position : " + startPos);
        timer = intervalTime;
    }

    private void Update()
    {
        if (spawnByTime)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                Spawn();
                timer = intervalTime;
            }
        }
    }

    public void Spawn()
    {
        for (int i = 0; i < objectPrefabs.Count; i++)
        {
            if (!objectPrefabs[i].activeInHierarchy)
            {
                float _startPosX = Random.Range(spawnPosition.x, spawnPosition.y);
                objectPrefabs[i].transform.position = new Vector3(_startPosX, startPos.y, 0);

                objectPrefabs[i].SetActive(true);
                break;
            }

        }
    }

    public void Despawn(GameObject obj)
    {
        for (int i = 0; i < objectPrefabs.Count; i++)
        {
            if (objectPrefabs[i] == obj)
            {
                objectPrefabs[i].SetActive(false);
                Vector3 pos = gameObject.transform.localPosition;
                objectPrefabs[i].transform.localPosition = new Vector3(pos.x, pos.y, pos.z);
                break;
            }
        }
    }

    void StartSpawning()
    {
        spawnByTime = true;
    }

    void OnGameOver()
    {
        spawnByTime = false;
        DespawnAll();
    }

    void DespawnAll()
    {
        for (int i = 0; i < objectPrefabs.Count; i++)
        {
            if (objectPrefabs[i].activeInHierarchy)
            {
                objectPrefabs[i].SetActive(false);

            }
        }
    }
}
