﻿using UnityEngine;
using System.Collections;

public class DragSwipeObject : MonoBehaviour
{
    // Public Variables
    public bool gameStart;

    // Private Variables
    private float deltaX, deltaY;
    private Rigidbody2D playerRigid2D;

    public void OnEnable()
    {
        EndlessRunnerGameController.onStartGame += OnGameStart;
    }

    public void OnDisable()
    {
        EndlessRunnerGameController.onStartGame -= OnGameStart;
    }

    private void Start()
    {
        playerRigid2D = GetComponent<Rigidbody2D>();
    }

    private void OnGameStart()
    {
        gameStart = true;
    
    }
    private void Update()
    {
        if(gameStart)
        {
       
            if(Input.touchCount > 0)
            {
                Touch _touch = Input.GetTouch(0);
                Vector2 touchPosition = Camera.main.ScreenToWorldPoint(_touch.position);

                switch(_touch.phase)
                {
                    case TouchPhase.Began:
                    deltaX = touchPosition.x - transform.position.x;
                    deltaY = touchPosition.y - transform.position.y;
                    break;

                    case TouchPhase.Moved:
                    playerRigid2D.MovePosition(new Vector2(touchPosition.x - deltaX, touchPosition.y - deltaY));
                    break;

                    case TouchPhase.Ended:
                    playerRigid2D.velocity = Vector2.zero;
                    break;

                }
            }
        }
    }
}
