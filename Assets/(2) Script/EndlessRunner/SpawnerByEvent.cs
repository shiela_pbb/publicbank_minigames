﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerByEvent : MonoBehaviour
{
    // Public Variables
    public GameObject prefab;
    public int prefabCount;

    // Private Variables
    private List<GameObject> prefabList;

    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }

    private void OnDestroy()
    {

    }

    public void Start()
    {
        prefabList = new List<GameObject>();
        for (int i = 0; i < prefabCount; i++)
        {
            GameObject obj = Instantiate(prefab, gameObject.transform);
            obj.SetActive(false);
            prefabList.Add(obj);
        }

        ObjectDespawner.onObjectDespawn += OnSpawn;
    }

    GameObject Spawn()
    {

        for (int i = 0; i < prefabList.Count; i++)
        {
            if (!prefabList[i].activeInHierarchy)
            {
                prefabList[i].SetActive(true);

                return prefabList[i];
            }
        }

        return null;
    }

    public void OnSpawn(GameObject obj)
    {
        if (obj == null)
            return;

        if (!obj.tag.Contains("Asteroid"))
            return;

        Vector3 pos = obj.transform.position;
        GameObject _prefab = Spawn();
        _prefab.transform.position = new Vector3(pos.x, pos.y, pos.z);
        AudioManager.instance.PlaySFX("explosion_1");
    }

}
