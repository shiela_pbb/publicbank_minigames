﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoPosAnim : MonoBehaviour
{
    public Vector3 showPosition;
    public Vector3 hidePosition;
    public Vector3 movePosition;
    public float animationDuration;
    public bool snap;
    public bool OnEventAnimationComplete;
    public bool isHUD;
    public bool movePositionOnSpawn;
    public bool hasDelay;
    public float delayDuration;


    public delegate void OnCardSpawnedDone();
    public static event OnCardSpawnedDone onCardSpawnedDone;

    public delegate void OnHUDReady();
    public static event OnHUDReady onHUDReady;

    public delegate void OnAnimationComplete();
    public static event OnAnimationComplete onAnimationComplete;

    public delegate void OnAddScore();
    public static event OnAddScore onAddScore;

    private void Awake()
    {
        if(isHUD)
            GameController.onDisplayHUD += OnShowHUD;
        else
            if(!movePositionOnSpawn)
                GameController.onSpawnCards += OnSpawnCards;

                
    }

    public void OnEnable()
    {
        if (movePositionOnSpawn)
        {
            DoAnimate(movePosition);
        }
    }

    private void OnDisable()
    {

    }

    private void OnDestroy()
    {

    }

    void OnShowHUD()
    {
        DoAnimate(showPosition);
        GameController.onDisplayHUD -= OnShowHUD;
    }

    void OnSpawnCards()
    {
        DoAnimate(showPosition);
        GameController.onSpawnCards -= OnSpawnCards;
        if(OnEventAnimationComplete)
            AudioManager.instance.PlaySFX("shuffle_cards_4");
    }

    void OnHide()
    {
        DoAnimate(hidePosition);
    }

    void DoAnimate(Vector3 pos)
    {
        if (!hasDelay)
            gameObject.transform.DOLocalMove(pos, animationDuration, snap).OnComplete(OnCompleteAnimation);
        else
            StartCoroutine( DelayAnimation(pos));
    }

    IEnumerator DelayAnimation(Vector3 pos)
    {
        yield return new WaitForSeconds(delayDuration);
        gameObject.transform.DOLocalMove(pos, animationDuration, snap).OnComplete(OnCompleteAnimation);

    }

    void OnCompleteAnimation()
    {
        if (!OnEventAnimationComplete)
            return;

        if (!movePositionOnSpawn)
        {
            if (!isHUD)
            {
                if (onCardSpawnedDone != null)
                {
                    onCardSpawnedDone.Invoke();
                }
            }
            else
            {

                if (onHUDReady != null)
                {
                    onHUDReady.Invoke();
                }
            }

        }
        else
        {

            if (onAnimationComplete != null)
            {
                onAnimationComplete.Invoke();
            }
            // TODO : notify score and GameController

            if (onAddScore != null)
            {
                onAddScore.Invoke();
                AudioManager.instance.PlaySFX("Add_coin_8");
            }
        }
    }
}
