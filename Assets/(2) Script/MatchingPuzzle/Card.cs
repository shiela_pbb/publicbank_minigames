﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class Card : MonoBehaviour
{
    // Public Variables
    public Image front;
    public Image back;
    public Image blocker;
    public int value;

    // Private Variables

    private void Awake()
    {
        FlipFront(false);
    }

    public void FlipFront(bool flip)
    {
        if (flip)
        {
            // Reveal
            
            front.enabled = true;
            blocker.enabled = true;
            back.enabled = false;

        }
        else
        {
            // Hide
            front.enabled = false;
            blocker.enabled = false;
            back.enabled = true;
        }
    }
}
