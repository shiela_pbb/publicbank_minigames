﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CardSelect : MonoBehaviour
{
    // Private Variables
    public bool isCardSelected = true;
    public bool isLocked;

    // Private Variable


    public delegate void OnCardSelected(Card card);
    public static event OnCardSelected onCardSelected;

    public delegate void OnCardDeselected(Card card);
    public static event OnCardDeselected onCardDeselected;

    public void OnEnable()
    {
        GameController.onStartGame -= ResetCard;
        GameController.onCardReset -= ResetCard;

        PlayAgain();
    }

    public void OnDisable()
    {
        GameController.onStartGame -= ResetCard;
        GameController.onCardReset -= ResetCard;
        GameController.onPlayAgain -= PlayAgain;

        PlayAgain();
    }

    public void Start()
    {
        GameController.onStartGame += ResetCard;
        GameController.onCardReset += ResetCard;
        GameController.onPlayAgain += PlayAgain;
    }

    public void OnCardSelect(GameObject obj)
    {
        //Debug.Log("GameController.instance.gameStatus : " + GameController.instance.gameStatus);
        if(GameController.instance.gameStatus == GameController.GameStatus.Initializing)
            return;

        //Debug.Log("isLocked : " + isLocked);
        if (isLocked)
            return;

        //Debug.Log("GameController.instance.gameCardStatus : " + GameController.instance.gameCardStatus);
        //Debug.Log("GameController.instance.gameCardStatus : " + GameController.instance.gameStatus);
        if (GameController.instance.gameCardStatus == GameController.GameCardStatus.Animating 
            && GameController.instance.gameStatus != GameController.GameStatus.Playing
            || GameController.instance.gameStatus == GameController.GameStatus.GameOver)
            return;

        //Debug.Log("obj.GetComponent<CardSelect>().isCardSelected : " + obj.GetComponent<CardSelect>().isCardSelected);
        if (obj.GetComponent<CardSelect>().isCardSelected)
            return;

        //Debug.Log("OnCardSelect(GameObject obj)");
        obj.GetComponent<CardSelect>().isCardSelected = true;

        Card c = obj.GetComponent<Card>();
        GameController.instance.CardSelected(c);       
    }

    public void OnDeselectCard()
    {
        isCardSelected = false;
        if (onCardDeselected != null)
        {
            onCardDeselected.Invoke(this.gameObject.GetComponent<Card>());
        }
    }

    public void ResetCard()
    {
        if(GameController.instance.gameStatus == GameController.GameStatus.GameOver)
            isLocked = false;
            
        if (isLocked)
            return;

        isCardSelected = false;
        gameObject.GetComponent<Button>().interactable = true;
        //Debug.Log("interactable : " + gameObject.GetComponent<Button>().interactable);
    }

    public void LockCard()
    {
        isLocked = true;
    }

    public void PlayAgain()
    { 
        isLocked = false;
        isCardSelected = true;

        gameObject.GetComponent<Card>().FlipFront(false);
    }

}
