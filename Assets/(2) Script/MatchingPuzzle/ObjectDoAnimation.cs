﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectDoAnimation : MonoBehaviour
{


    public enum AnimationStatus
    {
        Ready,
        Reveal,
        Hide,
        MoveNewPos,
        MoveInitPos,
        RevealFlipDone,
        HideFlipDOne,
        MoveToNewPositionDone,
        MoveToInitPositionDone

    }

    public delegate void OnMoveToNewPosition();
    public static event OnMoveToNewPosition onMoveToNewPositionDone;

    public delegate void OnMoveInitPositionDone();
    public static event OnMoveInitPositionDone onMoveInitPositionDone;


    // Public Variables
    public Card card;
    public GameObject centerPositon;
    public Vector3 NewPosition;
    public Vector3 doPosition;
    public Vector3 doRotation;
    public Vector3 doScale;
    public Vector3 initilizePosition;
    public bool sendEventOnInitialize;

    // Privste Variables
    AnimationStatus animStatus;

    private void OnEnable()
    {

    }

    private void OnDisable()
    {
        GameController.onCardInitializing -= OnDoMoveToInitializePosition;
    }

    private void Start()
    {
        animStatus = AnimationStatus.Ready;

        GameController.onCardInitializing += OnDoMoveToInitializePosition;
        GameController.onCardStartFlipReveal += CardFlipReveal;
        GameController.onPlayAgain += PlayAgain;

        GameController.onCardReset += ResetCardAnimation;

        if (card == null)
        {
            card = this.gameObject.GetComponent<Card>();
        }
    }

    public void PlayAgain()
    {
        animStatus = AnimationStatus.Ready;

        GameController.onCardInitializing += OnDoMoveToInitializePosition;
        GameController.onCardStartFlipReveal += CardFlipReveal;
        GameController.onPlayAgain += PlayAgain;

        GameController.onCardReset += ResetCardAnimation;

        if(card == null)
        {
            card = this.gameObject.GetComponent<Card>();
        }
    }

    public void OnCardSelected (Card _card)
    {
        if (animStatus != AnimationStatus.Ready)
            return;

        if (card.gameObject == _card.gameObject)
        {
            doRotation = new Vector3(0f, 180f, 0f);
            FlipCard(doRotation);
            card.FlipFront(true);
        }

    }

    public void CardFlipReveal()
    {
        //Debug.Log(" CardFlipReveal : " + animStatus);
        if (animStatus != AnimationStatus.Ready)
            return;

        card.FlipFront(true);
        animStatus = AnimationStatus.Reveal;

        doRotation = new Vector3(0f, 180f, 0f);

        FlipCard(doRotation);
        AudioManager.instance.PlaySFX("flip_card_1");
        GameController.onCardStartFlipReveal -= CardFlipReveal;
        GameController.onCardStartFlipHide += CardFlipHide;

    }

    public void CardFlipHide()
    {
        //Debug.Log("CardFlipHide");
        if (animStatus != AnimationStatus.RevealFlipDone || gameObject.GetComponent<CardSelect>().isLocked)
            return;

        card.FlipFront(false);

        animStatus = AnimationStatus.Hide;
        doRotation = new Vector3(0f, -180f, 0f);
        FlipCard(doRotation);
        AudioManager.instance.PlaySFX("draw_card_1");
        GameController.onCardStartFlipHide -= CardFlipHide;
    }

    public void OnDoMoveToInitializePosition()
    {
        animStatus = AnimationStatus.MoveInitPos;
        initilizePosition = centerPositon.transform.position;
        MovePosition(initilizePosition);
    }

    public void OnDoMoveToNewPosition()
    {
        animStatus = AnimationStatus.MoveNewPos;
        Vector3 pos = new Vector3(doPosition.x, doPosition.y, doPosition.z);
        MovePosition(doPosition);
    }

    public void ResetCardAnimation()
    {
        animStatus = AnimationStatus.Ready;
    }


    private void FlipCard(Vector3 rotation)
    {
        card.gameObject.transform.DORotate(rotation, 0.5f, RotateMode.LocalAxisAdd).OnComplete(OnAnimationDone).SetAutoKill();
    }

    private void MovePosition(Vector3 position)
    { 
        card.gameObject.transform.DOMove(position, 0.4f, true).OnComplete(OnAnimationDone).SetAutoKill();

    }


    void OnAnimationDone()
    {
        if (animStatus == AnimationStatus.Reveal)
        {
            animStatus = AnimationStatus.RevealFlipDone;

            if (GameController.instance.gameCardStatus == GameController.GameCardStatus.Animating)
            {
                GameController.instance.gameCardStatus = GameController.GameCardStatus.Ready;
                //Debug.Log(" OnEnableOverlay(false);");
                GameController.instance.OnEnableOverlay(false);
            }

            if (GameController.instance.gameStatus == GameController.GameStatus.Initializing)
            {
                CardFlipHide();
            }

            if (GameController.instance.gameStatus == GameController.GameStatus.Playing || GameController.instance.gameStatus == GameController.GameStatus.GameOver)
            {
                if (GameController.instance.gameCardStatus == GameController.GameCardStatus.CheckCard)
                {
                    GameController.instance.CompareCards();
                }
            }

        }

        if (animStatus == AnimationStatus.Hide)
        {
            animStatus = AnimationStatus.HideFlipDOne;
            //Debug.Log("GameController.instance.gameStatus : " + GameController.instance.gameStatus);

            if (GameController.instance.gameStatus == GameController.GameStatus.Initializing)
            {
                StartCoroutine(InitializeEvent());
            }

            if (GameController.instance.gameStatus == GameController.GameStatus.Playing)
            {
                if (GameController.instance.gameCardStatus == GameController.GameCardStatus.Animating)
                {
                    GameController.instance.gameCardStatus = GameController.GameCardStatus.Ready;
                    GameController.instance.OnSetCardStatus();
                    animStatus = AnimationStatus.Ready;
                }
            }
        }

        if (animStatus == AnimationStatus.MoveNewPos)
        {
            animStatus = AnimationStatus.MoveToNewPositionDone;

            //Debug.Log("AnimationStatus.MoveToNewPositionDone");
            if (GameController.instance.gameStatus == GameController.GameStatus.Initializing)
            {
                if (sendEventOnInitialize)
                {
                    if (onMoveToNewPositionDone != null)
                    {
                        onMoveToNewPositionDone.Invoke();
                        //Debug.Log("GameController.instance.gameStatus : " + GameController.instance.gameStatus);
                    }
                }
            }

            animStatus = AnimationStatus.Ready;

        }

        if (animStatus == AnimationStatus.MoveInitPos)
        {
            animStatus = AnimationStatus.MoveToInitPositionDone;
            //Debug.Log("GameController.instance.gameStatus : " + GameController.instance.gameStatus);

            if (GameController.instance.gameStatus == GameController.GameStatus.Initializing)
            {
                if (onMoveInitPositionDone != null)
                {
                    StartCoroutine(InitializeEvent());
                }
            }
        }

    }

    IEnumerator InitializeEvent()
    {
        yield return new WaitForSeconds(0.4f);
        if (animStatus == AnimationStatus.HideFlipDOne)
        {
            OnDoMoveToInitializePosition();
        }
        else if (animStatus == AnimationStatus.MoveToInitPositionDone)
        {
            onMoveInitPositionDone.Invoke();
        }

    }


}
