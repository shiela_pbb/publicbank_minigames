﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // Public Varialbes
    public TMP_Text timer;
    public Image pulse;
    public float timeValue;

    public float deductValue;
    public bool isPlaying;

    //Private Variables
    private float time;

    public delegate void OnGameOver();
    public static event OnGameOver onGameOver;

    // Start is called before the first frame update
    private void OnEnable()
    {
        if (pulse != null)
            pulse.enabled = false;
    }

    public void Start()
    {
        // TODO : listen to gamecontroller when to initialize the game
        InitializeTimer();
        GameController.onPlayAgain += InitializeTimer;
    }

    void InitializeTimer()
    {
        GameController.onStartGame -= StartTimer;
        GameController.onStartGame += StartTimer;
        GameController.onDeductTime -= DeductTime;
        GameController.onDeductTime += DeductTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(isPlaying)
        {
            time -= Time.deltaTime;
            timer.text = Mathf.RoundToInt(time).ToString();


            if (time <= 0)
            {
                GameController.onStartGame -= StartTimer;
                GameController.onDeductTime -= DeductTime;
                if (onGameOver != null)
                {
                    onGameOver.Invoke();

                }
                isPlaying = false;
                timer.text = "0";

            }
        }
    }

    public void StartTimer ()
    {
        RestartTimer();
        isPlaying = true;
    }

    public void RestartTimer ()
    {
        if (timeValue <= 0)
        {
            if (onGameOver != null)
            {
                onGameOver.Invoke();
            }
        }

        time = timeValue;
    }

    public void DeductTime ()
    {

        if (pulse != null)
        {
            pulse.enabled = true;
            StartCoroutine(OnPulseEnable());
        }

        time -= deductValue;
    }

    IEnumerator OnPulseEnable()
    {
        yield return new WaitForSeconds(0.2f);
        pulse.enabled = false;
    }

}
