﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public enum GameStatus
    {
        Ready,
        Initializing,
        Start,
        Playing,
        GameOver

    }

    public enum GameCardStatus
    {
        Ready,
        Animating,
        CheckCard,
        GameOver

    }

    public delegate void OnDisplayHUD();
    public static event OnDisplayHUD onDisplayHUD;

    public delegate void OnSpawnCards();
    public static event OnSpawnCards onSpawnCards;


    public delegate void OnCardStartFlipReveal();
    public static event OnCardStartFlipReveal onCardStartFlipReveal;

    public delegate void OnCardStartFlipHide();
    public static event OnCardStartFlipHide onCardStartFlipHide;

    public delegate void OnCardInitializing();
    public static event OnCardInitializing onCardInitializing;

    public delegate void OnStartGame();
    public static event OnStartGame onStartGame;

    public delegate void OnSpawnGameObject(Vector3 position);
    public static event OnSpawnGameObject onSpawnGameObject;

    public delegate void OnCardReset();
    public static event OnCardReset onCardReset;

    public delegate void OnDeductTime();
    public static event OnDeductTime onDeductTime;

    public delegate void OnPlayAgain ();
    public static event OnPlayAgain onPlayAgain;
    // Public Variables
    public Randomizer randomizer;
    public GameStatus gameStatus;
    public GameCardStatus gameCardStatus;
    public Button playButton;
    public Image overlay;
    public GameObject gameHud;
    public Image gameOver;


    // Private Variables
    private List<Card> cards;
    private List<Card> lockedCards;

    public static GameController instance = null;
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

    }

    private void OnEnable()
    {
        gameStatus = GameStatus.Ready;
        DoPosAnim.onHUDReady += OnHUDShow;
    }

    private void OnDisable()
    {
        
    }

    private void OnDestroy()
    {

    }

    public void Start()
    {
        cards = new List<Card>();
        lockedCards = new List<Card>();

        if (onDisplayHUD != null)
            onDisplayHUD.Invoke();

        gameCardStatus = GameCardStatus.Ready;
    }

    public void Initialize()
    {

        playButton.gameObject.SetActive(false);
        //playButton.GetComponentInChildren<TMP_Text>().text = "Initializing";

        gameStatus = GameStatus.Initializing;

        CardsStartFlipReveal();
        ObjectDoAnimation.onMoveInitPositionDone += MoveToNewPosition;
    }

    public void MoveToNewPosition()
    {
        randomizer.RandomNewPosition();

        ObjectDoAnimation.onMoveInitPositionDone -= MoveToNewPosition;
        ObjectDoAnimation.onMoveToNewPositionDone += StartGame;
    }

    public void CardsStartFlipReveal()
    {
        if (onCardStartFlipReveal != null)
        {
            onCardStartFlipReveal.Invoke();
            gameCardStatus = GameCardStatus.Animating;
        }
    }

    public void CardsStartFlipHide()
    {
        if (gameCardStatus == GameCardStatus.Ready)
        { 
            if (onCardStartFlipHide != null)
            {
                gameCardStatus = GameCardStatus.Animating;
                onCardStartFlipHide.Invoke();
            }
        }
    }

    public void StartGame()
    {
        //Debug.Log("++++++++++++++++ StartGame ");
        gameStatus = GameStatus.Playing;

        gameCardStatus = GameCardStatus.Ready;

        Timer.onGameOver += GameOver;

        if (onStartGame != null)
        {
            onStartGame.Invoke();
        }

        //Debug.Log(" OnEnableOverlay(false);");
        OnEnableOverlay(false);

        //playButton.GetComponentInChildren<TMP_Text>().text = "Playing";
        //playButton.interactable = false;
       

        CardSelect.onCardSelected += CardSelected;

        //Debug.Log(" gameStatus = GameStatus.Playing; : " + gameStatus);
    }

    public void CardSelected(Card card)
    {
        if (gameCardStatus == GameCardStatus.Ready)
        {
            //Debug.Log(" CardSelected gameCardStatus : " + gameCardStatus);
            //Debug.Log(" OnEnableOverlay(true);");
            OnEnableOverlay(true);
            cards.Add(card);

            card.GetComponent<ObjectDoAnimation>().CardFlipReveal();
            gameCardStatus = GameCardStatus.Animating;

            //Debug.Log(" cards.Count : " + cards.Count);
            if (cards.Count == 2)
            {
                gameCardStatus = GameCardStatus.CheckCard;
            }
        }

    }

    public void CompareCards()
    {
        List<int> values = new List<int>();

        for ( int i = 0; i < cards.Count; i++)
        {
            values.Add(cards[i].value);
        }

        gameCardStatus = GameCardStatus.Ready;

        if (values[0] == values[1])
        {
            if (onSpawnGameObject != null)
            {
                onSpawnGameObject.Invoke(cards[0].transform.localPosition);
                AudioManager.instance.PlaySFX("correct_2");
            }

            for(int i = 0; i < cards.Count; i++)
            {
                cards[i].GetComponent<CardSelect>().LockCard();
                lockedCards.Add(cards[i]);
            }
        } 
        else
        {
            CardsStartFlipHide();
            if (onDeductTime != null)
            {
                onDeductTime.Invoke();
            }
            AudioManager.instance.PlaySFX("error_1");
            
        }

        cards.Clear();
        //Debug.Log(" OnEnableOverlay(false);");
        OnEnableOverlay(false);
    }

    public void GameOver()
    {
        AudioManager.instance.PlaySFX("game_over_1");
        OnEnableOverlay(true);
        //playButton.GetComponentInChildren<TMP_Text>().text = "Game Over!";
        //gameOver.enabled = true;
        gameOver.gameObject.transform.DOLocalMove(new Vector3(0f, 0f, 0f), 0.3f, true).OnComplete(OnGameOver);

        gameCardStatus = GameCardStatus.GameOver;
        Timer.onGameOver -= GameOver;
        CardsStartFlipReveal();
    }

    void OnGameOver()
    {
        //gameHud.SetActive(false);
    }

    public void OnSetCardStatus()
    {
        OnEnableOverlay(false);

        if (onCardReset != null)
        {
            onCardReset.Invoke();
        }
    }

    public void OnEnableOverlay(bool enable)
    {
        if(overlay == null)
        {
            Image obj = GameObject.Find("overlay").GetComponent<Image>();
            overlay = obj;
        }
            
        if (!enable && overlay.enabled)
            overlay.enabled = false;
        else if(enable && !overlay.enabled)
            overlay.enabled = true;/**/
    }

    public void OnHUDShow()
    {
        if (onSpawnCards != null)
            onSpawnCards.Invoke();

        DoPosAnim.onHUDReady -= OnHUDShow;
        DoPosAnim.onCardSpawnedDone += OnCardSpawned;
    }

    public void OnCardSpawned()
    {
        DoPosAnim.onCardSpawnedDone -= OnCardSpawned;
        Debug.Log(" OnEnableOverlay(false);");
        OnEnableOverlay(false);
    }

    public void PlayAgain()
    {
        Debug.Log(" Game controller button pressed play again");
        OnEnableOverlay(true);
        cards.Clear();
        gameOver.gameObject.transform.DOLocalMove(new Vector3(0f, 2500f, 0f), 0.3f, true);

        if(onPlayAgain != null)
        {
            onPlayAgain.Invoke();
        }

        playButton.gameObject.SetActive(true);
        playButton.interactable = true;
        gameStatus = GameStatus.Ready;
    }

}
