﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Score : MonoBehaviour
{

    // Public Variables
    public TMP_Text score;
    public int scoreCount;
    public int scoreToAdd;

    // Private Variables
    private void OnDisable()
    {
        DoPosAnim.onAddScore -= AddScore;
    }

    private void OnDestroy()
    {
        DoPosAnim.onAddScore -= AddScore;
    }
    public void Start()
    {
        InitializeScore();
        GameController.onPlayAgain -= InitializeScore;
        GameController.onPlayAgain += InitializeScore;
    }

    public void InitializeScore()
    {
        score.text = "0";
        scoreCount = 0;
        DoPosAnim.onAddScore -= AddScore;
        DoPosAnim.onAddScore += AddScore;
        //Debug.Log("Initialize and add listener ");

        Timer.onGameOver += OnGameOver;

        GameController.onPlayAgain -= InitializeScore;
    }

    public void AddScore()
    {
        //Debug.Log("AddScore : " + scoreToAdd);
        scoreCount += scoreToAdd;
        //Debug.Log("scoreCount : " + scoreCount);
        score.text = scoreCount.ToString();
    }

    void OnGameOver()
    {
        DoPosAnim.onAddScore -= AddScore;
        Timer.onGameOver -= OnGameOver;
        GameController.onPlayAgain += InitializeScore;
    }

}
