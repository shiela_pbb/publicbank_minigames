﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : MonoBehaviour
{
    //Public Variables
    public List<GameObject> cards;
    public List<GameObject> positions;
    public List<Vector3> newPositionList;

    //Private Variables

    
    public void RandomNewPosition()
    {
        AudioManager.instance.PlaySFX("shuffle_cards_7");
        newPositionList = new List<Vector3>();
        List<Vector3> list = new List<Vector3>();

        foreach (GameObject obj in positions)
        {
            list.Add(obj.transform.position);
        }

        while (list.Count!= 0)
        {
            int ranDomNumber = Random.Range(0, list.Count);
            newPositionList.Add(list[ranDomNumber]);
            list.Remove(list[ranDomNumber]);
        }

        for (int i = 0; i < cards.Count; i++)
        {
            ObjectDoAnimation anim = cards[i].GetComponent<ObjectDoAnimation>();
            anim.doPosition = newPositionList[i];
            anim.OnDoMoveToNewPosition();
        }

    }
}
