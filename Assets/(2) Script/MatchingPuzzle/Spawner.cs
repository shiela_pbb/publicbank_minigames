﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    //Public Variables
    public GameObject prefab;
    public RectTransform parent;
    public int numberOfPrefabToSpawn;
    public Vector3 offsetPosition;

    //Private Variables
    private List<GameObject> objectPrefabs;

    // Start is called before the first frame update
    void Start()
    {
        GameController.onSpawnGameObject += Spawn;

        objectPrefabs = new List<GameObject>();
        for (int i = 0; i < numberOfPrefabToSpawn; i++)
        {
            GameObject obj = (GameObject) Instantiate(prefab, parent);
            obj.SetActive(false);
            objectPrefabs.Add(obj);
        }
    }

    public void Spawn(Vector3 position)
    {
        DoPosAnim.onAnimationComplete += Despawn;

        for ( int i = 0; i < objectPrefabs.Count; i++)
        {
            if (!objectPrefabs[i].activeInHierarchy)
            {
                objectPrefabs[i].transform.localPosition = position - offsetPosition;
                objectPrefabs[i].SetActive(true);
                break;
            }

        }
    }

    void Despawn()
    {
        for ( int i = 0; i < objectPrefabs.Count; i++)
        {
            if (objectPrefabs[i].activeInHierarchy)
            {
                objectPrefabs[i].SetActive(false);
                Vector3 pos = gameObject.transform.localPosition;
                objectPrefabs[i].transform.localPosition = new Vector3(pos.x, pos.y, pos.z);
                DoPosAnim.onAnimationComplete -= Despawn;
                break;
            }
        }
    }
}
