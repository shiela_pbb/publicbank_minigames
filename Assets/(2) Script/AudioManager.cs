﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource bgm;
    public AudioSource sfx;

    public static AudioManager instance = null;

    private void Awake()
    {
        if(instance == null)
            instance = this;
        else if(instance != this)
            Destroy(gameObject);
    }

    public void Start()
    {
        bgm.Play();
    }

    public void PlaySFX(string clip)
    {
        sfx = gameObject.transform.Find(clip).GetComponent<AudioSource>();
        //Debug.Log("sfx : " + sfx);
        sfx.Play();
    }

    public void PlayBGM(string clip)
    {

    }
}
